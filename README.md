<div>
        <h4>validation class that provides fast validation of email, password, name and age.</h4>
        <h5>You need to include only one file <a href="e.fixit.in/validation.js">http://e-fixit.in/validation.js</a>
        </h5>
        <b>
            <h4 style="text-decoration: underline;">Now You have access 6 class</h4>
        </b>
        <ul>
            <li>Validation()</li>
            <li>EmailVal()</li>
            <li>PasswordVal()</li>
            <li>NameVal()</li>
            <li>AgeVal()</li>
            <li>Helper()</li>
            <h6>You can call Validtion.help('provide name of function') or call Helper.email_val()</h6>
        </ul>
        <h6>You need to create the object and only pass the value like email, password etc.</h6>
        <h6>You can also see help menu that provide you details about each function</h6>
        <b>
            <h4 style="text-decoration: underline;">Utility Function</h4>
        </b>
        <ul>
            <li>1.areSame(str1, str2);
                <h6>it takes two string and return true if same.</h6>
            </li>
            <li>2.regexTester(pattern,str);
                <h6>it take regex as first parameter and second as string and return true if string satified given
                    regex.</h6>
            </li>
            <li>3.isValidObject(object,array_keys);
                <h6> it takes two argumen one is object and other is array of key if some of array key are present in
                    object then return true.</h6>
            </li>
            <li>4.isDate(str);
                <h6>it return if given input is date.</h6>
            </li>
            <li>5.passwordStrengthChecker(password,strenght,lenght);
                <h6>it return true if given password follow given strength like 'strong'.</h6>
            </li>
            <h6>You can call Utility function by call class name e.g Validation.areSame('str1','str2');</h6>
        </ul>
    </div>